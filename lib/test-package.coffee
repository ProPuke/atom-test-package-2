TestPackageView = require './test-package-view'
{CompositeDisposable} = require 'atom'

module.exports = TestPackage =
  testPackageView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->
    @testPackageView = new TestPackageView(state.testPackageViewState)
    @modalPanel = atom.workspace.addModalPanel(item: @testPackageView.getElement(), visible: false)

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'test-package:toggle': => @toggle()

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @testPackageView.destroy()

  serialize: ->
    testPackageViewState: @testPackageView.serialize()

  toggle: ->
    console.log 'TestPackage was toggled!'

    if @modalPanel.isVisible()
      @modalPanel.hide()
    else
      @modalPanel.show()
